<?php



/*
 * Complete the 'urna' function below.
 *
 * The function is expected to return a CHARACTER.
 * The function accepts CHARACTER_ARRAY votos as parameter.
 */

function urna($votos) {
    // Write your code here
    $contagem = array_count_values($votos);
    return $contagem[0];
}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$votos_count = intval(trim(fgets(STDIN)));

$votos = array();

for ($i = 0; $i < $votos_count; $i++) {
    $votos_item = rtrim(fgets(STDIN), "\r\n")[0];
    $votos[] = $votos_item;
}

$result = urna($votos);

fwrite($fptr, $result . "\n");

fclose($fptr);
