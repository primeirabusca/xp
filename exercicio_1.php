<?php

/*
 * Complete the 'converteTemperatura' function below.
 *
 * The function is expected to return a FLOAT.
 * The function accepts following parameters:
 *  1. FLOAT temperatura
 *  2. STRING escalaOrigem
 *  3. STRING escalaDesejada
 */

function converteTemperatura($temperatura, $escalaOrigem, $escalaDesejada) {
    // Write your code here

    $conversao = $escalaOrigem . '.' .  $escalaDesejada;

  
    switch($conversao){
        case 'fahrenheit.celsius':
            $saida = (($temperatura - 32) / 1.8);
            break;
        case 'fahrenheit.kelvin':
            $saida = (($temperatura - 32) / 1.8 + 273.15);
            break;
        case 'kelvin.fahrenheit':
            $saida = (($temperatura - 273.15) * 1.8 + 32);
            break;
            case 'kelvin.celsius':
            $saida = ($temperatura - 273.15);
            break;
        case 'celsius.kelvin':
            $saida = ($temperatura + 273.15);
            break;
        case 'celsius.fahrenheit':
            $saida = (($temperatura * 1.8) + 32);
            break;      
    }
    
    
    return $saida;

}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");
$temperatura = rtrim(fgets(STDIN), "\r\n");
$escalaOrigem = rtrim(fgets(STDIN), "\r\n");
$escalaDesejada = rtrim(fgets(STDIN), "\r\n");


fwrite($fptr, converteTemperatura($temperatura,$escalaOrigem,$escalaDesejada) . "\n");
fclose($fptr);