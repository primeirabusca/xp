<?php



/*
 * Complete the 'tripleTheChances' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts INTEGER_ARRAY chances as parameter.
 */

function tripleTheChances($chances) {
    // Write your code here
    return array_map(
        function ($valores){
            return $valores * 3;
        }, $chances);
}

$fptr = fopen(getenv("OUTPUT_PATH"), "w");

$chances_count = intval(trim(fgets(STDIN)));

$chances = array();

for ($i = 0; $i < $chances_count; $i++) {
    $chances_item = intval(trim(fgets(STDIN)));
    $chances[] = $chances_item;
}

$result = tripleTheChances($chances);

fwrite($fptr, implode("\n", $result) . "\n");

fclose($fptr);